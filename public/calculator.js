function calc() {
  var price = Number(document.getElementById("price").value);
  var n = Number(document.getElementById("n").value);
  var result = price * n;
  if (Number.isNaN(result)) {
    document.getElementById("result").innerHTML = "Enter numbers please!";
  }
  else {
    document.getElementById("result").innerHTML = result;
  }
}
